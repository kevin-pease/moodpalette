#!/bin/bash

# This script builds and runs the backend in a Docker container locally.
sudo systemctl start docker
sudo docker build -t mpbackend .         
sudo docker run --name mpbackend -d -p 3000:3000 mpbackend