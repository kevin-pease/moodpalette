const express = require("express");
const router = express.Router();

// Define routes
router.get("/helloworld", (req, res) => {
  res.send("Hello, World!");
});

module.exports = router;
