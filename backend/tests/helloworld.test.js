const request = require("supertest");
const server = require("../app");

describe("GET /helloworld", () => {
  it('responds with "Hello, World!"', async () => {
    const response = await request(server).get("/helloworld");
    expect(response.status).toBe(200);
    expect(response.text).toBe("Hello, World!");
  });
});

// After all tests are completed, close the server
afterAll((done) => {
  server.close(done);
});
