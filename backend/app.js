const express = require("express");
const routes = require("./routes");

const app = express();
const port = 3000;

app.use("/", routes);

const server = app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});

module.exports = server;
