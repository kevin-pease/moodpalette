#!/bin/bash

# This script builds and runs the frontend in a Docker container locally.
ng build --configuration=production
sudo systemctl start docker
sudo docker build -t mpfrontend .         
sudo docker run --name mpfrontend -d -p 80:80 mpfrontend 